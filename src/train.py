from keras.optimizers import Adam
from keras.models import Sequential
from keras.layers import (Dense, Reshape, Flatten, Conv2D, Conv2DTranspose,
    LeakyReLU, Dropout)
from keras.utils.vis_utils import plot_model
import numpy as np
from matplotlib import pyplot
from keras.datasets.fashion_mnist import load_data
from numpy import vstack

def define_discriminator(input_shape=(28,28,1)):
    model = Sequential()
    model.add( Conv2D(64, (3,3), strides=(2,2) ,padding='same', input_shape=input_shape,name='disc_1'))
    model.add(LeakyReLU(alpha=0.2,name='disc_2'))
    model.add(Dropout(rate=0.4,name='disc_3'))
    model.add(Conv2D(64, (3,3), strides=(2,2) ,padding='same', input_shape=input_shape,name='disc_4'))
    model.add(LeakyReLU(alpha=0.2,name='disc_5'))
    model.add(Dropout(rate=0.4,name='disc_6'))
    model.add(Flatten(name='disc_7'))
    model.add(Dense(1,activation='sigmoid',name='disc_8'))
    opt = Adam(lr=0.0002,beta_1=0.5)
    model.compile(optimizer=opt,loss='binary_crossentropy', metrics=['accuracy'])
    return model

def define_generator(latent_dim):
    model = Sequential()
    model.add(Dense( 128 * 7 * 7 , input_dim=latent_dim, name='gen_1'))
    model.add(Reshape((7,7,128),name='gen_2'))
    model.add(Conv2DTranspose(128,(4,4),strides=(2,2),padding='same',name='gen_3'))
    model.add(LeakyReLU(alpha=0.2,name='gen_4'))
    model.add(Conv2DTranspose(128,(4,4),strides=(2,2),padding='same',name='gen_5'))
    model.add(LeakyReLU(alpha=0.2,name='gen_6'))
    model.add(Conv2D(1,(7,7),activation='sigmoid',padding='same',name='gen_7'))
    return model

def define_gan(generator,discriminator):
    model = Sequential()
    #discriminator.trainable = False
    model.add(generator)
    model.add(discriminator)
    opt = Adam(lr=0.0002,beta_1=0.5)
    model.compile(optimizer=opt,loss='binary_crossentropy')
    return model

def load_real_samples():
    (trainX, _), (_, _) = load_data()
    X=trainX.astype('float32')
    X = X/255.0
    return X

def generate_real_samples(trainX,half_batch):
    selected = np.random.randint(0,len(trainX),half_batch)
    expanded = list()
    for index in selected:
        expanded.append(  np.expand_dims(trainX[index],-1))
    y = np.ones((half_batch,1))
    expanded = np.reshape(expanded,(half_batch,28,28,1))
    return expanded, y

def generate_latent_points(latent_dim, n_samples):
    x_input = np.random.randn(latent_dim * n_samples)
    x_input = x_input.reshape(n_samples,latent_dim)
    return x_input

def generate_fake_samples(generator,latent_dim, n_samples):
    x_input = generate_latent_points(latent_dim, n_samples)
    X = generator.predict(x_input)
    y = np.zeros((n_samples,1))
    return X,y

def train_gan(generator,discriminator,gan,latent_dim,epochs,batch_size,dataset):
    half_batch = int(batch_size/2)
    for i in range(epochs):
        for j in range(batch_size):
            print("Epcoch:{} Batch:{}".format(i,j))
            for layer in discriminator.layers:
                layer.trainable = True
            print('__ Discriminator layers __')
            for layer in discriminator.layers:
                print(layer.name, layer.trainable)
            print('________________')
            x_real, y_real = generate_real_samples(dataset,half_batch)
            x_fake, y_fake = generate_fake_samples(generator,latent_dim,half_batch)
            X, y = vstack((x_real, x_fake)), vstack((y_real, y_fake))
            discriminator.train_on_batch(X,y)
            #discriminator.train_on_batch(x_real,y_real)
            #discriminator.train_on_batch(x_fake,y_fake)
            x_gan = generate_latent_points(latent_dim,batch_size)
            y_gan = np.ones((batch_size,1))

            for layer in discriminator.layers:
                layer.trainable = False
            gan.train_on_batch(x_gan,y_gan)
            print('__ GAN layers __')
            for model in gan.layers:
                for layer in model.layers:
                    print(layer.name, layer.trainable)
            print('________________')

discriminator = define_discriminator()
#disc.summary()
generator = define_generator(100)
#gen.summary()
gan = define_gan(generator,discriminator)



latent_dim = 100
batch_size = 256
epochs=100

train_gan(generator,discriminator,gan,latent_dim,epochs,batch_size,dataset)
n_samples=10
fake_images, _ = generate_fake_samples(generator,latent_dim,n_samples)
#real_images = generate_real_samples(trainX,n_samples)
#print()
for i in range(n_samples):
    #r = np.reshape(real_images[i],(28,28))
    f = np.reshape(fake_images[i],(28,28))
    pyplot.subplot(5,5, i + 1)
    #pyplot.imshow(r,cmap='gray_r')
    #pyplot.subplot(2,10, i + 11)
    pyplot.imshow(f,cmap='gray_r')
pyplot.show()
dataset = load_real_samples()