# GAN MNIST digits dataset

This is mostly a copy from Jason Brownlee GAN example using MNSIT dataset. Here is the link to his [book](
https://machinelearningmastery.com/generative_adversarial_networks)

My only change was disabling the line where weights in the discriminator are made not trainable inside the  _define_gan_ function:
```
    #d_model.trainable = False
```

I instead handled how the discriminator is or isn't trained in the _train_gan_ function:
- I made the discriminator trainable to learn real and 'fake' images:
```
    for layer in discriminator.layers:
        layer.trainable = True
    print('__ Discriminator layers __')
    for layer in discriminator.layers:
        print(layer.name, layer.trainable)
    print('________________')
    x_real, y_real = generate_real_samples(dataset,half_batch)
    x_fake, y_fake = generate_fake_samples(generator,latent_dim,half_batch)
    X, y = vstack((x_real, x_fake)), vstack((y_real, y_fake))
```

- Then I made the discriminator **non** trainable when it is used in a part of the gan model so only the generator part is updated:
```
    x_gan = generate_latent_points(latent_dim,batch_size)
    y_gan = np.ones((batch_size,1))

    for layer in discriminator.layers:
        layer.trainable = False
    gan.train_on_batch(x_gan,y_gan)
```

This how generated images look like after 100 epochs and 256 batch size.

![trained](images/trained_digits.png)

This is the shared link to the Google colaboratory notebook:
[colab](https://colab.research.google.com/drive/1tp-9TvppGoNsL6U0eCh4CaDfIm_6ULm4?usp=sharing)